# Reveal.JS quick-starter template

## Getting Started

For instance, if you wanted to write a talk about "Rake and Ruby builds", you could i.e. run:

```bash
$ git clone https://gitlab.com/jamietanna/revealjs-starter revealjs-starter
$ mkdir rake_ruby_builds
$ cd rake_ruby_builds
$ ln -s ../revealjs-starter/Rakefile
$ rake 'init[rake_ruby_builds_slides, Rake and Ruby builds]'
```

Which will then generate your file structure and template out a `bower.json` and `package.json`, which are only used for dependency management, and a `config.yml` to define how to build your slides.

Note that this will also download dependencies, so will require a network connection. It is safe to cancel the step at this point, i.e. if you do not have network, as the file structure has been created at this point.
