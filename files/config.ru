use Rack::Static,
  :urls => ['/img', '/bower_components', '/casts', '/markdown', '/asciinema-player.2.5.0.css', '/asciinema-player.2.5.0.js'],
  :root => 'out'

run lambda { |env|
  [
    200,
    {
      'Content-Type'  => 'text/html',
      'Cache-Control' => 'public, max-age=86400'
    },
    File.open('out/slides.html', File::RDONLY)
  ]
}
