## Came for the Campus, Stayed for the Community

## Prerequisites

This project requires the front-end dependency management system, `bower`, which can be installed via `npm install`.

Once run, you'll need to pull those dependencies, by running `./node_modules/bower/bin/bower install`.

Next, you need to install the Ruby dependencies, which requires Ruby's [bundler dependency management tool](http://bundler.io/) and then can run `bundle install`.

## Building

Make changes to the files within the `slides/` folder, and then run `bundle exec rake`. That's it! The generated `slides.html` will exist in the `out/` folder.

## Running

Out-of-the-box you can run `bundle exec rackup` which will serve the `out/` folder at `http://localhost:9292`.
