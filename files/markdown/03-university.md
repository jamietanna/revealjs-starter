### University

---

<img src="img/compsoc.png" alt="" />

Note:
- more just social
- bit of a clique
- difficult to get involved

---

<img src="img/joenash.png" alt="" />

Note:
- approached about co-founding Hacksoc

---

<img src="img/hacksoc.png" alt="" />

Note:
- hobbyist programming and hackathon society
- non-Compsci
- sharing technical passion and learnings
- competitive programming
