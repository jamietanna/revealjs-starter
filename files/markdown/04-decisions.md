## Decisions

Note:
- When the time came for moving on from university, I was fortunate enough to have a few choices.

---

### Location

---

Network

---

https://nottingham.digital

Note:
- diverse

---

<img src="img/emma-andrew.png" alt="" />

Note:
These awesome people!

---

### Do I want a graduate scheme?

---

Development support

---

Ownership

---

### My Experiences

Note:
- ingrained in team
- not "a grad"
- having an awesome time
