require 'erubis'
require 'yaml'

def write_eruby(eruby_filename, variables, output_filename)
  input = File.read(eruby_filename)
  eruby = Erubis::Eruby.new(input)

  out = eruby.result(variables)
  File.open(output_filename, 'w') do |file|
    file.write(out)
  end
end

desc 'Initialise the file structure for a new Reveal.JS project'
task :init, [:package_name, :slides_title] do |_t, args|
  raise 'Package name cannot be empty' if args[:package_name].to_s.empty?
  raise 'Package name cannot contain spaces' if args[:package_name].to_s.match?(/ /)
  raise 'Slides title cannot be empty' if args[:slides_title].to_s.empty?

  # Folder structure {{{
  FileUtils.mkdir_p 'casts'
  FileUtils.mkdir_p 'img'
  FileUtils.mkdir_p 'slides'
  # }}}

  # Static files {{{
  %w(Gemfile Gemfile.lock config.ru .gitignore slides.html.erb).each do |file|
    File.symlink("../revealjs-starter/files/#{file}", file)
  end
  # }}}

  # One-time generated files {{{
  variables = {
    package_name: args[:package_name],
    slides_title: args[:slides_title]
  }
  write_eruby('../revealjs-starter/templates/bower.json.erb', variables, 'bower.json')
  write_eruby('../revealjs-starter/templates/config.yml.erb', variables, 'config.yml')
  write_eruby('../revealjs-starter/templates/package.json.erb', variables, 'package.json')
  # }}}

  # Folder structure for generated files {{{
  FileUtils.mkdir_p 'out'
  FileUtils.mkdir_p 'out/markdown'
  File.symlink('../bower_components', 'out/bower_components')
  File.symlink('../casts', 'out/casts')
  File.symlink('../img', 'out/img')

  File.symlink('../../revealjs-starter/files/asciinema-player.2.5.0.css', 'out/asciinema-player.css')
  File.symlink('../../revealjs-starter/files/asciinema-player.2.5.0.js', 'out/asciinema-player.js')
  # }}}

  # install dependencies {{{
  sh 'npm install'
  # by going through the `node_modules` directory, we don't need to manipulate
  # PATH or install globally
  sh './node_modules/bower/bin/bower install'
  sh 'bundle install --path vendor/bundle'
  # }}}
end

desc 'Compile templates to markdown'
task :compile do
  Dir.glob('slides/*.md.erb').each do |filename|
    in_content = File.read(filename)
    out_filename = File.basename(filename, '.erb')
    out_filename = "out/markdown/#{out_filename}"

    FileUtils.mkdir_p 'out/markdown'
    out_content = Erubis::Eruby.new(in_content).result()
    File.open(out_filename, 'w') do |file|
      file.write(out_content)
    end
  end
end

desc 'Build Slides'
task :build do
  variables = YAML.load_file('config.yml')
  write_eruby('slides.html.erb', variables, 'out/slides.html')
end

task default: ['compile', 'build']
